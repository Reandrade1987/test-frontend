import React from 'react';
import Pesquisar from './Pesquisar';
import youtube from '../apis/youtube';
import Lista from './Lista';
import Descricao from './Descricao';
import '../style/style.css';


class App extends React.Component {
    state = {
        videos: [],
        selected: null
    }
    handleSubmit = async (pesquisar) => {
        const response = await youtube.get('/search', {
            params: {
                q: pesquisar
            }
        })
        this.setState({
            videos: response.data.items
        })
    };
    SelectVideo = (video) => {
        this.setState({selected: video})
    }

    render() {
        return (
            <div className='container'>
                <Descricao video={this.state.selected}/>
                <Pesquisar handleFormSubmit={this.handleSubmit}/>
                <Lista SelectVideo={this.SelectVideo} videos={this.state.videos}/>
            </div>
        )
    }
}

export default App;