import React from 'react';
import Items from './Items';

const Lista = ({videos , SelectVideo}) => {
    const renderedVideos =  videos.map((video) => {
        return <Items key={video.id.videoId} video={video} SelectVideo={SelectVideo} />
    });

    return <div>{renderedVideos}</div>;
};
export default Lista;