import React from 'react';
import '../style/style.css';


const Descricao = ({video}) => {
    if (!video) {
        return <h4 style={{ marginTop: '40px' }}>Pesquise aqui...</h4>;
    }

    const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`;
    return (
        <div>
            <div className='iframe'>
                <iframe src={videoSrc} allowFullScreen title='Video player' />
            </div>
            <div className=''>
                <h4>{video.snippet.title}</h4>
                <p>{video.snippet.description}</p>
            </div>
        </div>

    )
}

export default Descricao;