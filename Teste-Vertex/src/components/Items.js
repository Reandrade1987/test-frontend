import React from 'react';
import '../style/style.css';

const Items = ({video , SelectVideo}) => {
    return (
        <div onClick={ () => SelectVideo(video)} className=' video-item item'>
            <img className='ui image' src={video.snippet.thumbnails.medium.url} alt={video.snippet.description}/>
            <div className='content'>
                <div className='title '>{video.snippet.title}</div>
            </div>
        </div>
    )
};
export default Items;