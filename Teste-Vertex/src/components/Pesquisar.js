import React from 'react';

class Pesquisar extends React.Component {
    state = {
        pesquisar: '',
        error: 'Campo vazio! Preencha o campo para fazer a pesquisa'
    };
    handleChange = event => { 
        const {pesquisar} = this.state
        this.setState({
            pesquisar: event.target.value
        });
        if( pesquisar === "" ){
            this.setState({error: 'Campo vazio! Preencha o campo para fazer a pesquisa'})
        }else {
            this.setState({error: ""})
        }
    };

    handleSubmit = event => {
        event.preventDefault();
        this.props.handleFormSubmit(this.state.pesquisar);
    }
    render() {
        return (
            <div className='search-bar ui segment'>
                <form onSubmit={this.handleSubmit} className='ui form'>
                    <input onChange={this.handleChange} placeholder='Pesquisar...' name='pesquisar-video' type="text" value={this.state.pesquisar}/>
                    <button class="ui purple button">Buscar</button>
                </form>
                <h2 style={{ color: 'red', fontSize: '12px' }}>{this.state.error}</h2>
            </div>
        )
    }
}

export default Pesquisar;